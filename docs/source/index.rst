.. PySatsearch documentation master file, created by
   sphinx-quickstart on Thu Apr 16 19:17:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PySatsearch documentation
=======================================

.. toctree::
   :maxdepth: 2



Example code
============

The following is a simple example script for pulling all the products in the AIT/Service category ::

   from PySatsearch import Satsearch
   from dotenv import load_dotenv
   import os

   load_dotenv('.env')
   satsearch = Satsearch(os.getenv('APP_ID'), os.getenv('API_TOKEN'), debug=False, cache=False)
   products = satsearch.get_all_products('AIT/Service')  # This is a pandas DataFrame
   print(products)



class Satsearch
===============

 This is the main class that holds all the functions of the PySatsearch library.

 To initialize it, you must pass the app-id and api-token, and you may specify the column width, where you to call
 one of the printing functions, and the debug mode:
 satsearch = Satsearch(app_id, api_token, column_width=20, debug=False)


 We recommend that you use environment variables to store the app-id and api-token.
 You'll want to create a .env file structured as follows: ::

     APP_ID='<insert your app-id here>'
     API_TOKEN='<insert your api-token here>'

 Make sure to insert you app-id and api-tokens inside the quotation marks.
 Then, you'll want to install the python-dotenv library.
 The most popular method to install python packages is thought pip: ::

     python3 -m pip install python-dotenv

 Then, use this boilerplate code: ::

     import os
     from dotenv import load_dotenv


     load_dotenv('.env')
     app_id = os.getenv('APP_ID')
     api_token = os.getenv('API_TOKEN')

 We recommend using environment variables rather than hard-coding because it makes it easier to share you code,
 without having to worry about disclosing your private api-token and app-id.

 Once you have your api-token and app-id, you can initialize the class and start using the library: ::

     satsearch = Satsearch(app_id, api_token, column_width=20, debug=False, cache=False)


__init__
^^^^^^^^

To initialize the class, you must pass the app_id, api_token, and can pass the column_width and the debug
boolean, and whether or not one wishes to cache the parts. ::

   satsearch = Satsearch(app_id, api_token, column_width=20, debug=False, cache=False)

get_product
^^^^^^^^^^^

This function allows the user to get information about a specific product for which the uuid is known. ::

   product_info = satsearch.get_product(uuid, return_type='raw', cache_timeout=datetime.timedelta(days=1))

If the return type is set to pandas, the product return will be in the form of a pandas DataFrame. This can
be easily concatenated with other DataFrames using: ::

   pd.concat([product_1_DataFrame, product_1_DataFrame], sort=False)

If the return_type is set to simple, this function will return a dictionary with an entry for each attribute.
Each attribute will have a value and a unit.

If the return type is raw, the data returned will be raw data received from the database.

If cache was enabled in the initialization of this class, then the cash_timeout will determine how long after
creation, the cache is deemed to be invalid. The cache can also be deleted manually (by default it is in the
'__PySatsearchCache__' folder).


get_all_products
^^^^^^^^^^^^^^^^

This function returns all the products for a given uuid or category (or subcategory) name. Note that subcategory
names are not unique, and hence for subcategories it is recommended to use the category uuid, which can be found
by calling the "get_categories()" function. ::

   get_all_products(self, category_uuid_or_name, return_type='pandas')

The default value for the keyword argument is pandas, in which case all the products from that category are
concatenated into on large pandas DataFrame.

If instead the return_type kwarg is set to raw, a list with the all the products in the format given by the
server is retuned.


get_product_list
^^^^^^^^^^^^^^^^

This function returns a list of the products for which the given parameters match.
The parameters that can be added are category_uuid_or_name, supplier_uuid, product_name, supplier_name and page. The
value for page can be set to either a positive nonzero integer, or 'all'.

If a supplier_uuid is provided, then any value provided to the supplier_name will be ignored. ::

   product_list = satsearch.get_product_list(self, category_uuid_or_name=None, supplier_uuid=None,
                   product_name=None, supplier_name=None, page=None)

If there are no errors, a list will be returned. Each entry of the list will be of the format: ::

   product_list = {
       "uuid": uuid,
       "name": name,
       "images": images_url_list,
       "supplier_name": supplier_name,
       "product_url": product_url,
       "supplier_url": supplier_url,
       "supplier_uuid": supplier_uuid,
       "category_uuid": category_uuid,
       "last_modified": last_modified
   }

get_supplier_list
^^^^^^^^^^^^^^^^^

This function will return a list of the suppliers. By default, it will return only the ten first suppliers.
To obtain a page other than the first, set the keyword argument page to another number. To obtain all suppliers,
, set page to 'all'. If however, you wish to look for a specific supplier, you can pass that name to the
supplier_name parameter. This will filter the results on supplier name (pattern matching). ::

   supplier_list = satsearch.get_supplier_list(page=1, supplier_name, return_type='raw')

The default return_type is 'raw', the response will then be a list. Each entry of the list will be of the form: ::

   supplier_list[i] = {"uuid": uuid,
                       "name": name,
                       "last_modified": last_modified
                       }

If the return type is set to 'pandas', the same information is returned but in a pandas DataFrame.

get_categories
^^^^^^^^^^^^^^
This function will return the data such that it is easy to find the correct category uuid. ::

   categories = satsearch.get_categories()

The data returned is the raw data as called from the /categories endpoint of the api.

print_categories
^^^^^^^^^^^^^^^^
This function prints the category names and their uuid's in a hierarchical manner: ::

   satsearch.print_categories()