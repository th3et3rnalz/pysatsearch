import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="PySatsearch",
    version="0.1.0",
    author="Joe Verbist for satsearch",
    author_email="joe@joeverbist.com",
    description="Python library for satsearch API",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/satseach/pysatsearch",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
