# def simplify(attribute_dict):
#     if attribute_dict["value"] != "/":
#         val = attribute_dict["value"]
#     elif attribute_dict["min-value"] != "/" and attribute_dict["max-value"] != "/":
#         lowest = float(attribute_dict["min-value"])
#         highest = float(attribute_dict["max-value"])
#         val = (lowest + highest) / 2
#     elif attribute_dict["min-value"] != "/":
#         val = attribute_dict["min-value"]
#     elif attribute_dict["max-value"] != "/":
#         val = attribute_dict["max-value"]
#     else:
#         print("Hey, there is an attribute with no data, there might be something wrong")
#         return None
#
#     # if the answer isn't text, then we should convert it to SI units
#     unit = attribute_dict['measurement_unit']
#     try:
#         val = float(val)
#         # TIME   TIME   TIME   TIME   TIME
#         if unit == 'ms':
#             # We're going to seconds
#             val = val * 1000
#             unit = 's'
#         elif unit == 'min':
#             # We're going to seconds
#             val = val * 60
#             unit = 's'
#         elif unit == 'hr':
#             # We're going to seconds
#             val = val * 3600
#             unit = 's'
#         elif unit == 'dy':
#             # We're going to seconds
#             val = val * 86400
#             unit = 's'
#         elif unit == 'wk':
#             # We're going to seconds
#             val = val * 604800
#             unit = 's'
#         elif unit == 'yr':
#             # We're going to seconds
#             val = val * 31557600
#             unit = 's'
#
#         # FREQUENCY   FREQUENCY   FREQUENCY
#         elif unit == 'KHz':
#             # We're going to Hz
#             val = val * 10 ** 3
#             unit = 'Hz'
#         elif unit == 'MHz':
#             # We're going to Hz
#             val = val * 10 ** 6
#             unit = 'Hz'
#         elif unit == 'GHz':
#             # We're going to Hz
#             val = val * 10 ** 9
#             unit = 'Hz'
#
#         #  DISTANCE  DISTANCE  DISTANCE
#         elif unit == 'mm':
#             # We're going to meter
#             val = val / 1000
#             unit = 'm'
#         elif unit == 'cm':
#             # We're going to meter
#             val = val / 100
#             unit = 'm'
#         elif unit == 'in':
#             # We're going to meter
#             val = val / 39.37008
#             unit = 'm'
#         elif unit == 'km':
#             # We're going to meter
#             val = val * 1000
#             unit = 'm'
#
#         # POWER  POWER  POWER  POWER  POWER
#         elif unit == 'mW':
#             # We're going to W
#             val = val / 1000
#             unit = 'W'
#         elif unit == 'kW':
#             # We're going to W
#             val = val * 1000
#             unit = 'W'
#
#         # FORCE  FORCE  FORCE  FORCE  FORCE
#         elif unit == 'µN':
#             # We're going to N
#             val = val * 10 ** -6
#             unit = 'N'
#         elif unit == 'mN':
#             # We're going to N
#             val = val * 10 ** -3
#             unit = 'N'
#
#         # TORQUE  TORQUE  TORQUE  TORQUE  TORQUE
#         elif unit == 'mN m':
#             val = val * 10 ** -3
#             unit = 'N m'
#
#     except ValueError:
#         unit = 'string'
#
#     return val, unit


class Category:
    def __init__(self, uuid, name, parent_uuid, children_list=None, level=0):
        self.uuid = uuid
        self.name = name
        self.parent_uuid = parent_uuid

        self.level = level  # the higher the level, the more of a subcategory it is
        self.children_list = children_list
        self.children = {}
        self.generate_children()

    def generate_children(self):
        for child in self.children_list:
            assert child['parent_uuid'] == self.uuid
            self.children[child['name']] = Category.from_dict(child, level=self.level + 1)

    @classmethod
    def from_dict(cls, dct, level=0):
        return cls(uuid=dct['uuid'],
                   name=dct['name'],
                   parent_uuid=dct['parent_uuid'],
                   children_list=dct['children'],
                   level=level)

    # def find_uuid_from_name(self, name):
    #     if name == self.name:
    #         return [self.uuid, self.level]
    #     for child in self.children:
    #         return_value = child.find_uuid_from_name(name)
    #
    #         # The following two lines are present such that if a result is found in a low level, the answer
    #         # is breakfasted up.
    #         if type(return_value) is list:
    #             return return_value
    #     return False

    def __repr__(self):
        return f"<Category. name:{self.name}, uuid:{self.uuid}>"

    def find_uuid_from_names(self, category_uuid_or_name):
        if category_uuid_or_name is None:
            return None
        backwards_slash = category_uuid_or_name.split('\\')
        forward_slash = category_uuid_or_name.split('/')
        if len(forward_slash) > 1 or len(backwards_slash) > 1:
            if len(forward_slash) > 1:
                names = forward_slash
            else:
                names = backwards_slash

            category_uuid = self.search(names)
        else:
            category_uuid = self.search([category_uuid_or_name])

        # We must now validate that if the passed category_uuid_or_name was not found (in which case it was simply)
        # returned by the search function) it is a valid category_uuid. Otherwise we must throw an Exception.
        if category_uuid == category_uuid_or_name:
            if self.verify_uuid(category_uuid_or_name) is None:
                raise Exception("Pass a valid category uuid or name")

        return category_uuid

    def search(self, name_list):
        if len(name_list) == 1:
            try:
                return self.children[name_list[0]].uuid
            except KeyError:
                # This happens when the name is not in the categories, which can happen when what is passed is not a
                # name, but a category uuid. The we return the category uuid. We will later validate that it is a
                # valid category uuid.
                return name_list[0]
        try:
            return self.children[name_list[0]].search(name_list[1:])
        except KeyError:
            # This happens when the name is not in the categories, which can happen when what is passed is not a
            # name, but a category uuid, with a slash in it somehow. The we return the category uuid. We will later
            # validate that it is a valid category uuid.
            return name_list[0]

    def verify_uuid(self, uuid):
        # This function checks all the categories to find out if the category uuid matches any.
        real_uuid = False
        if self.uuid == uuid:
            return True
        for _, child in self.children.items():
            if child.verify_uuid(uuid):
                return True

    def print_sub(self):
        print(f"{'   '*self.level} {self.name}: {self.uuid}")
        for child_name, child in self.children.items():
            child.print_sub()
