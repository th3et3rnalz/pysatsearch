import pandas as pd


class SatsearchPart:
    def __init__(self,
                 uuid,
                 name,
                 images,
                 configurations,
                 subcomponents,
                 last_modified,
                 supplier_name,
                 summary,
                 product_url,
                 supplier_url,
                 attributes,
                 raw):
        self.uuid = uuid
        self.name = name
        self.images = images
        self.configurations = configurations
        self.subcomponents = subcomponents
        self.last_modified = last_modified
        self.supplier_name = supplier_name
        self.summary = summary
        self.product_url = product_url
        self.supplier_url = supplier_url
        self.attributes = attributes
        self.raw = raw

        self.attribute_dict = {}
        self.df = None

    @classmethod
    def from_dict(cls, dct):
        return cls(uuid=dct['uuid'],
                   name=dct['name'],
                   images=dct['images'],
                   configurations=dct['configurations'],
                   subcomponents=dct['subcomponents'],
                   last_modified=dct['last_modified'],
                   supplier_name=dct['supplier_name'],
                   summary=dct['summary'],
                   product_url=dct['product_url'],
                   supplier_url=dct['supplier_url'],
                   attributes=dct['attributes'],
                   raw=dct)

    def to_pandas(self):
        attributes_names = ['uuid', 'name']
        attributes_value = [self.uuid, self.name]
        for el in self.attributes:
            value, unit = self.simplify_attribute(el)
            if el['class']['name'] + f" [{unit}]" not in attributes_names:
                attributes_names.append(el['class']['name'] + f" [{unit}]")
                attributes_value.append(value)

        self.df = pd.DataFrame([attributes_value], columns=attributes_names)
        self.df.set_index('uuid', inplace=True)

        return self.df

    def to_raw(self):
        return self.raw

    def to_simple(self):  # Returns the same data as to_pandas, but in dict format
        for el in self.attributes:
            value, unit = self.simplify_attribute(el)
            name = el['class']['name']
            self.attribute_dict[name] = {'value': value,
                                         'unit': unit}

        return self.attribute_dict

    @staticmethod
    def simplify_attribute(attribute_dict):
        ad = attribute_dict  # The logical variable name is just too long
        unit = ad['measurement_unit']
        if len(ad['value']) > 0:
            try:
                value = float(ad['value'])
            except ValueError:
                value = ad['value']
        elif len(ad['minimum_value']) > 0 and len(ad['maximum_value']) > 0:
            try:
                min_val = float(ad['minimum_value'])
                max_val = float(ad['maximum_value'])
            except ValueError:
                print("Error in part attribute", attribute_dict)
                raise ValueError
            value = (min_val + max_val) / 2
        elif len(ad['minimum_value']) > 0:
            try:
                value = float(ad['minimum_value'])
            except ValueError:
                value = ad['minimum_value']
        else:
            try:
                value = float(ad['maximum_value'])
            except ValueError:
                value = ad['maximum_value']

        return value, unit
