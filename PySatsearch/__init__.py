import requests
import os
import pickle
import re
from PySatsearch.helpers import Category
from PySatsearch.part import SatsearchPart
import pandas as pd
import datetime

"""
Welcome to PySatsearch, a library to access Satsearch's parts API.
If you have any questions, do not hesitate to contact us at info@satsearch.co

This library consists of the Satsearch class which is used for all the interaction with the API. With it, you can get 
product and supplier data as stipulated in satsearch's API specification at https://api.satsearch.co 


"""


class Satsearch:
    """
    This is the main class that holds all the functions of the PySatsearch library.

    To initialize it, you must pass the app-id and api-token, and you may specify the column width, where you to call
    one of the printing functions, and the debug mode:
    satsearch = Satsearch(app_id, api_token, column_width=20, debug=False)


    We recommend that you use environment variables to store the app-id and api-token.
    You'll want to create a .env file structured as follows:

        APP_ID='<insert your app-id here>'
        API_TOKEN='<insert your api-token here>'
    
    Make sure to insert you app-id and api-tokens inside the quotation marks.
    Then, you'll want to install the python-dotenv library.
    The most popular method to install python packages is thought pip:

        python3 -m pip install python-dotenv

    Then, use this boilerplate code:

        import os
        from dotenv import load_dotenv

    
        load_dotenv('.env')
        app_id = os.getenv('APP_ID')
        api_token = os.getenv('API_TOKEN')
    
    We recommend using environment variables rather than hard-coding because it makes it easier to share you code,
    without having to worry about disclosing your private api-token and app-id.

    Once you have your api-token and app-id, you can initialize the class and start using the library:

        satsearch = Satsearch(app_id, api_token, column_width=20, debug=False, cache=False)

    """

    # Setting the API access codes.
    def __init__(self, app_id, api_token, column_width=20, debug=False, cache=False):
        """
        To initialize the class, you must pass the app_id, api_token, and can pass the column_width and the debug
        boolean, and whether or not one wishes to cache the parts.
        
            satsearch = Satsearch(app_id, api_token, column_width=20, debug=False, cache=False)

        """
        # Just initializing some variables...
        self.app_id = app_id
        self.token = api_token
        self.column_width = column_width
        self.debug = debug
        self.cache = cache
        self.cache_folder_name = '__PySatsearchCache__'
        if os.getenv('STAGING') == 'True':
            self.base_url = "https://staging-api.satsearch.co/v1/"
        else:
            self.base_url = "https://api.satsearch.co/v1/"
        print(self.base_url)
        self.__version__ = '0.1.0'

        # Now, let's generate a user-accessible variable that contains all the user accessible functions.
        functions = [func for func in dir(self) if callable(getattr(self, func))]
        self.functions = [func for func in functions if func[0] != "_"]

    # ----------------------------------------------------------------------------------------------------------------
    #            FOR USER USAGE - TO BE CALLED BY THE USER  ||   FOR USER USAGE - TO BE CALLED BY THE USER
    # ----------------------------------------------------------------------------------------------------------------
    def get_product(self, uuid, return_type='pandas', cache_timeout=datetime.timedelta(days=1)):
        """
        This function allows the user to get information about a specific product for which the uuid is known.

            product_info = satsearch.get_product(uuid, return_type='raw', cache_timeout=datetime.timedelta(days=1))
        
        If the return type is set to pandas, the product return will be in the form of a pandas DataFrame. This can
        be easily concatenated with other DataFrames using:

            pd.concat([product_1_DataFrame, product_1_DataFrame], sort=False)

        If the return_type is set to simple, this function will return a dictionary with an entry for each attribute.
        Each attribute will have a value and a unit.

        If the return type is raw, the data returned will be raw data received from the database.

        If cache was enabled in the initialization of this class, then the cash_timeout will determine how long after
        creation, the cache is deemed to be invalid. The cache can also be deleted manually (by default it is in the
        __PySatsearchCache__ folder).
        """
        if self.cache:
            product = self.read_from_cache(uuid, cache_timeout)
            if not product:
                raw_data = self._call_url("products", uuid)
                product = SatsearchPart.from_dict(raw_data)
            self.store_object_in_cache(product)
        else:
            raw_data = self._call_url("products", uuid)
            product = SatsearchPart.from_dict(raw_data)

        if return_type == 'raw':
            return product.to_raw()
        elif return_type == 'pandas':
            return product.to_pandas()
        elif return_type == 'simplified':
            simple = product.to_simple()
            return simple
        return Exception("Invalid value for the return_type kwarg")

    def get_all_products(self, category_uuid_or_name, return_type='pandas'):
        """
        This function returns all the products for a given uuid or category (or subcategory) name. Note that subcategory
        names are not unique, and hence for subcategories it is recommended to use the category uuid, which can be found
        by calling the "get_categories()" function.

            get_all_products(self, category_uuid_or_name, return_type='pandas')

        The default value for the keyword argument is pandas, in which case all the products from that category are
        concatenated into on large pandas DataFrame.

        If instead the return_type kwarg is set to raw, a list with the all the products in the format given by the
        server is returned.
        """

        product_list = self.get_product_list(category_uuid_or_name=category_uuid_or_name, page='all')
        products = [self.get_product(product['uuid'], return_type='raw') for product in product_list]
        if return_type == 'raw':
            return products
        elif return_type == 'pandas':
            return self._to_pandas(products)
        return Exception("Invalid value for the return_type kwarg")

    def get_product_list(self, category_uuid_or_name=None, supplier_uuid=None, product_name=None, supplier_name=None,
                         page=None):
        """
        This function returns a list of the products for which the given parameters match.
        The parameters that can be added are category_uuid_or_name, supplier_uuid, product_name, supplier_name and page. The
        value for page can be set to either a positive nonzero integer, or 'all'.

        If a supplier_uuid is provided, then any value provided to the supplier_name will be ignored.

            product_list = satsearch.get_product_list(self, category_uuid_or_name=None, supplier_uuid=None,
                            product_name=None, supplier_name=None, page=None)
                        
        If there are no errors, a list will be returned. Each entry of the list will be of the format:

            product_list = {
                "uuid": uuid,
                "name": name,
                "images": images_url_list,
                "supplier_name": supplier_name,
                "product_url": product_url,
                "supplier_url": supplier_url,
                "supplier_uuid": supplier_uuid,
                "category_uuid": category_uuid,
                "last_modified": last_modified
         }

        """
        payload = []
        # This is an instance of the Category class, which holds all the categories
        main_category = self._get_categories()

        # The below value will be overwritten if it is a name, otherwise it is assumed to be a valid category uuid
        category_uuid = main_category.find_uuid_from_names(category_uuid_or_name=category_uuid_or_name)

        if category_uuid_or_name is not None:
            payload.append(['category_uuids', category_uuid])

        if supplier_uuid is not None:
            payload.append(['supplier_uuids', supplier_uuid])

        if product_name is not None:
            payload.append(['product_name', product_name])

        if supplier_name is not None:
            payload.append(['supplier_name', supplier_name])

        if page is not None and page != 'all':
            payload.append(['page', page])

        if self.debug:
            print("Payload:", payload)

        data = self._call_url(url_type='products', uuid=None, params=payload)

        if page == 'all':
            total = data['total']
            per_page = data['perPage']
            new_page_size = (total // per_page + 1) * per_page

            payload.append(['page_size', new_page_size])
            data = self._call_url(url_type='products', uuid=None, params=payload)

        return data['data']

    def get_supplier(self, uuid):
        """ 
        This function will return ...
        """

        raw_data = self._call_url('suppliers', uuid)
        return raw_data

    def get_supplier_list(self, page=1, supplier_name=None, return_type='raw'):
        """
        This function will return a list of the suppliers. By default, it will return only the ten first suppliers.
        To obtain a page other than the first, set the keyword argument page to another number. To obtain all suppliers,
        , set page to 'all'. If however, you wish to look for a specific supplier, you can pass that name to the
        supplier_name parameter. This will filter the results on supplier name (pattern matching).

            supplier_list = satsearch.get_supplier_list(page=1, supplier_name, return_type='raw')

        The default return_type is 'raw', the response will then be a list. Each entry of the list will be of the form:
        
            supplier_list[i] = {"uuid": uuid,
                                "name": name,
                                "last_modified": last_modified
                                }

        If the return type is set to 'pandas', the same information is returned but in a pandas DataFrame.
        """
        payload = []
        if supplier_name is not None:
            payload.append(['supplier_name', supplier_name])

        if page == 'all':
            # The first query is to find out how many suppliers there are
            first_page = self._call_url('suppliers?page=1', None, params=payload)
            total_pages = first_page['lastPage']
            per_page = first_page['perPage']
            page_size = (total_pages // per_page + 1) * per_page
            supplier_list = self._call_url(f'suppliers?page=1?page_size={page_size}', None, params=payload)['data']
        elif page == 1:
            supplier_list = self._call_url('suppliers?page=1', None, params=payload)['data']
        else:
            supplier_list = self._call_url('suppliers?page={}'.format(page), None, params=payload)['data']

        if return_type == 'pandas':
            keys = list(supplier_list[0].keys())
            return pd.DataFrame(supplier_list, columns=keys)
        elif return_type == 'raw':
            return supplier_list
        raise Exception('Pass valid return_type')

    def get_categories(self):
        """
        This function will return the data such that it is easy to find the correct category uuid.

            categories = satsearch.get_categories()

        The data returned will be of the format as returned from the \categories endpoint of the api.

        :return: dict
        """
        return self._call_url("products/categories", False)

    def print_categories(self):
        """
        This function prints the category names and their uuid's in a hierarchical manner:

            satsearch.print_categories()
        :return: None
        """

        categories = self._get_categories()
        categories.print_sub()

    # ----------------------------------------------------------------------------------------------------------------
    #      INTERNAL USAGE ONLY - NOT TO BE CALLED BY USER  ||   INTERNAL USAGE ONLY - NOT TO BE CALLED BY USER
    # ----------------------------------------------------------------------------------------------------------------
    def _call_url(self, url_type, uuid, params=None):
        # This is the core of the library. Here is where the api requests are made
        url = self.base_url + url_type
        if uuid is not False and uuid is not None:
            url = url + "/" + uuid
        if self.debug:
            print("URL called: {}".format(url))
        headers = {"Authorization": "Bearer " + self.token, "X-APP-ID": self.app_id}
        response = requests.get(url, headers=headers, params=params)
        if response.status_code != 200:
            print("ERROR code {}".format(response.status_code))
        return response.json()

    @staticmethod
    def _to_pandas(product_s):
        if type(product_s) == list:
            if len(product_s) == 0:
                return None
            products = []
            for el in product_s:
                stp = SatsearchPart.from_dict(el)
                products.append(stp.to_pandas())
            return pd.concat(products, sort=False)
        elif type(product_s) == dict:
            stp = SatsearchPart.from_dict(product_s)
            return stp.to_pandas
        raise Exception('Invalid datatype passed')

    def store_object_in_cache(self, product, filename=None):
        if isinstance(product, SatsearchPart):
            filename = product.uuid
        if self.cache_folder_name not in os.listdir():
            os.mkdir(self.cache_folder_name)
        else:
            if filename is None:
                raise Exception('No filename provided')
        os.chdir(self.cache_folder_name)
        f = open(filename + '.pickle', 'wb')
        pickle.dump(product, f)
        f.close()
        os.chdir('..')
        return True

    def read_from_cache(self, file, timeout):
        assert isinstance(timeout, datetime.timedelta)
        filename = file + '.pickle'
        if self.cache_folder_name not in os.listdir():
            return False
        os.chdir(self.cache_folder_name)
        if filename not in os.listdir():
            os.chdir('..')
            return False
        last_modified_datetime = datetime.datetime.fromtimestamp(os.path.getmtime(filename))
        if last_modified_datetime + timeout > datetime.datetime.now():
            f = open(filename, 'rb')
            product = pickle.load(f)
            f.close()
            os.chdir('..')
            return product
        os.chdir('..')
        return False

    def _get_categories(self):
        data = self.read_from_cache('categories', timeout=datetime.timedelta(minutes=5))
        if not data:
            data = self._call_url("products/categories", False)
            self.store_object_in_cache(data, filename='categories')

        if 'error' in data:
            raise Exception(data['error'])

        return Category(uuid=None, name='main', parent_uuid=None, children_list=data)



