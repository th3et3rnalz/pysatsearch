import unittest
import os
from dotenv import load_dotenv
from PySatsearch import Satsearch


class TestPySatsearch(unittest.TestCase):
    def test_loading(self):
        load_dotenv('.env')
        app_id = os.getenv('APP_ID')
        token = os.getenv('API_TOKEN')
        part = Satsearch(app_id, token)

        assert token is not None
        assert app_id is not None


if __name__ == '__main__':
    unittest.main()
