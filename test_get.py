import unittest
import os
from dotenv import load_dotenv
import pandas as pd
from PySatsearch import Satsearch


class TestPySatsearch(unittest.TestCase):
    def test_get_categories(self):
        # Some boilerplate
        load_dotenv('.env')
        app_id = os.getenv('APP_ID')
        token = os.getenv('API_TOKEN')
        satsearch = Satsearch(app_id, token)

        main_category = satsearch._get_categories()
        self.assertEqual(
            main_category.find_uuid_from_names(category_uuid_or_name='AIT/Service'),
            '67b824c4-2fef-505e-9d5b-53df6d97b270'
        )
        self.assertEqual(
            main_category.find_uuid_from_names(category_uuid_or_name='5f0dcc00-2427-5c64-86e4-1db2a6f21063'),
            '5f0dcc00-2427-5c64-86e4-1db2a6f21063'
        )
        self.assertRaises(
            Exception,
            main_category.find_uuid_from_names,
            'this_is_not_a_valid_uuid_or_name'
        )

    def test_get_product(self):
        # Some boilerplate
        load_dotenv('.env')
        app_id = os.getenv('APP_ID')
        token = os.getenv('API_TOKEN')
        satsearch = Satsearch(app_id, token)

        data = satsearch.get_product("15b09050-b756-51e8-85ac-7d809f4a42ef", return_type='simplified')
        simplified = {'mass': {'value': 0.33, 'unit': 'kg'},
                      'thrust': {'value': 0.62, 'unit': 'N'},
                      'catalyst': {'value': 'S-405', 'unit': ''},
                      'specific impulse': {'value': 216.5, 'unit': 's'},
                      'total thrust pulses': {'value': 275028.0, 'unit': ''},
                      'expansion ratio': {'value': 100.0, 'unit': ''},
                      'mass flow rate': {'value': 0.295, 'unit': 'g s^-1'},
                      'feed pressure': {'value': 16.900000000000002, 'unit': 'bar'},
                      'total impulse': {'value': 186000.0, 'unit': 'N s'},
                      'propellant type': {'value': 'hydrazine', 'unit': ''},
                      'chamber pressure': {'value': 14.649999999999999, 'unit': 'bar'}}

        assert data == simplified

        data = satsearch.get_product("15b09050-b756-51e8-85ac-7d809f4a42ef", return_type='pandas')

        self.assertIsInstance(data, pd.DataFrame)

        data = satsearch.get_product("15b09050-b756-51e8-85ac-7d809f4a42ef", return_type='raw')
        self.assertIsInstance(data, dict)

    def test_get_product_list(self):
        load_dotenv('.env')
        app_id = os.getenv('APP_ID')
        token = os.getenv('API_TOKEN')
        satsearch = Satsearch(app_id, token)

        data = satsearch.get_product_list(supplier_name="Honeywell Aerospace", page='all')
        self.assertEqual(data[0]['supplier_name'], 'Honeywell Aerospace Inc.')

        data = satsearch.get_product_list(supplier_uuid="5f82fcf9-1d67-50e1-bf38-185d3e305267")
        self.assertEqual(data[0]['supplier_name'], 'Honeywell Aerospace Inc.')

        data = satsearch.get_product_list(product_name="IFM Micro Thruster")
        self.assertEqual(data[0]['name'], "IFM Micro Thruster")

    def test_get_supplier(self):
        load_dotenv('.env')
        app_id = os.getenv('APP_ID')
        token = os.getenv('API_TOKEN')
        satsearch = Satsearch(app_id, token)

        data = satsearch.get_supplier("82ad45b3-165f-5053-99d8-fd9645588603")
        self.assertIsInstance(data, dict)
        self.assertEqual(list(data.keys()), ['uuid', 'name', 'last_modified', 'logo', 'summary', 'supplier_url'])

    def test_get_supplier_list(self):
        load_dotenv('.env')
        app_id = os.getenv('APP_ID')
        token = os.getenv('API_TOKEN')
        satsearch = Satsearch(app_id, token)

        data = satsearch.get_supplier_list(page='all', return_type='pandas')
        self.assertIsInstance(data, pd.DataFrame)
        self.assertEqual(data.shape[1], 3)

        data = satsearch.get_supplier_list(page=1, return_type='raw')
        self.assertEqual(len(data), 10)
        self.assertIsInstance(data, list)

        data = satsearch.get_supplier_list(page=1, return_type='raw', supplier_name='Honeywell')
        self.assertEqual(len(data), 2)

    def test_get_all_products(self):
        load_dotenv('.env')
        app_id = os.getenv('APP_ID')
        token = os.getenv('API_TOKEN')
        satsearch = Satsearch(app_id, token)

        products = satsearch.get_all_products('Power/Battery', return_type='pandas')  # This is a pandas DataFrame
        self.assertIsInstance(products, pd.DataFrame)


if __name__ == '__main__':
    unittest.main()
