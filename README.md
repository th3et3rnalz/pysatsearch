# PySatsearch

This library was written to interact with the satsearch API at api.satsearch.co. It provides a method for pulling parts from the satsearch database into python.

It currently is not available on PyPi, and must thus be downloaded and build and install manually To do this run:

    python3 setup.py sdist bdist_wheel
    python3 -m pip dist/PySatsearch-0.1.0-py3-none-any.whl
     
The main features of this library are direct exports to Pandas DataFrames and a rudimentary caching implementation (off by default).