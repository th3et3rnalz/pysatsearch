from PySatsearch import Satsearch
from dotenv import load_dotenv
import os

load_dotenv('.env')
satsearch = Satsearch(os.getenv('APP_ID'), os.getenv('API_TOKEN'), debug=False, cache=True)
products = satsearch.get_all_products('Power/Battery')  # This is a pandas DataFrame
print(products)
